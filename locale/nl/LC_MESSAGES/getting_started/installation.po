# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2021, 2022, 2023, 2024 Freek de Kruijf <freekdekruijf@kde.nl>
# SPDX-FileCopyrightText: 2022, 2025 Ronald Stroethoff <stroe43@zonnet.nl>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-23 01:46+0000\n"
"PO-Revision-Date: 2025-02-05 07:06+0100\n"
"Last-Translator: Ronald Stroethoff <stroet43@zonnet.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.5\n"

#: ../../getting_started/installation.rst:1
msgid "Kdenlive Documentation - How to install Kdenlive"
msgstr "Kdenlive documentatie - Hoe Kdenlive te installeren"

#: ../../getting_started/installation.rst:1
msgid ""
"KDE, Kdenlive, install, Installation, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, installeren, installatie, documentatie, "
"gebruikershandleiding, videobewerker, open-source, vrij, leren, gemakkelijk"

#: ../../getting_started/installation.rst:48
msgid "Installation"
msgstr "Installatie"

#: ../../getting_started/installation.rst:50
msgid ""
"Kdenlive is released about every three month. See the Kdenlive |news_page| "
"to get the latest information and release notes. And check out :doc:`/"
"more_information/whats_new` for the most recent feature additions."
msgstr ""
"Kdenlive wordt ongeveer elke drie maanden uitgegeven. Zie de Kdenlive |"
"news_page| om de laatste informatie en uitgavenotities te krijgen. En "
"bekijk :doc:`/more_information/whats_new` voor de meest recente toevoegingen "
"aan mogelijkheden."

#: ../../getting_started/installation.rst:52
msgid "You can install Kdenlive in two different ways:"
msgstr "U kunt een Kdenlive op twee verschillende manieren installeren:"

#: ../../getting_started/installation.rst:54
msgid "Using an installer"
msgstr "Door een installatie-programma te gebruiken"

#: ../../getting_started/installation.rst:55
msgid ""
"As a self-contained executable (Windows: *standalone*; Linux: *appimage*)."
msgstr ""
"Als een zelfstandige executable (Windows: *standalone*; Linux: *appimage*)."

#: ../../getting_started/installation.rst:57
msgid "On macOS you can only use the installer version."
msgstr "Bij macOS kunt u alleen de installatie-versie gebruiken."

#: ../../getting_started/installation.rst:59
msgid ""
"Visit the |download_page| of the Kdenlive web site for up to date "
"information on installing Kdenlive."
msgstr ""
"Bezoek de |download_page| van de Kdenlive website voor bijgewerkte "
"informatie over het installeren van Kdenlive."

#: ../../getting_started/installation.rst:61
msgid "You will find all previous Kdenlive versions in the |attic|."
msgstr "U kunt alle vorige versies van Kdenlive op de |attic| vinden."

#: ../../getting_started/installation.rst:66
msgid "Minimum System Requirements"
msgstr "Minimale systeemvereisten"

#: ../../getting_started/installation.rst:68
msgid ""
"**Operating system:** 64-bit Windows 7 or newer, Apple macOS 10.15 "
"(Catalina)\\ [1]_ or newer and on M1, 64-bit Linux. Please see the details "
"below."
msgstr ""
"**Besturingssysteem:** 64-bit Windows 7 of nieuwer, Apple macOS 10.15 "
"(Catalina)\\ [1]_ of nieuwer en op M1, 64-bit Linux. Zie de details "
"hieronder."

#: ../../getting_started/installation.rst:70
msgid ""
"**CPU:** x86 Intel or AMD; at least one 2 GHz core for SD video, 4 cores for "
"HD video, and 8 cores for 4K video. Please see the details below."
msgstr ""
"**CPU:** x86 Intel of AMD; minstens één 2 GHz kern voor SD video, 4 kernen "
"voor HD video en 8 kernen voor 4K video. Zie de details hieronder."

#: ../../getting_started/installation.rst:72
msgid ""
"**GPU:** OpenGL 2.0 that works correctly and is compatible. On Windows, you "
"can also use a card with good, compatible DirectX 9 or 11 drivers."
msgstr ""
"**GPU:** OpenGL 2.0 die correct werkt en compatibel is. Op Windows kunt u "
"ook een kaart gebruiken met goede, compatibele DirectX 9 of 11 "
"stuurprogramma's."

#: ../../getting_started/installation.rst:74
msgid ""
"**RAM:** At least 4 GB for SD video, 8 GB for HD video, and 16 GB for 4K "
"video."
msgstr ""
"**RAM:** Minstens 4 GB voor SD video, 8 GB voor HD video en 16 GB voor 4K "
"video."

#: ../../getting_started/installation.rst:76
msgid ""
"If your computer is at the lower end of CPU and RAM requirements, you should "
"use the :ref:`Preview Resolution <ui-monitors_preview_resolution>`, :doc:"
"`Proxy</getting_started/configure_kdenlive/configuration_proxy_clips>` and :"
"doc:`/tips_and_tricks/tips_and_tricks/timeline_preview_rendering` features "
"to help reduce preview lag."
msgstr ""
"Als uw computer in de buurt van de minimale eisen wat betreft CPU en RAM "
"zit, dan raden wij aan om de functionaliteiten :ref:`Resolutie "
"Voorbeeldweergave <ui-monitors_preview_resolution>`, :ref:`Proxy </"
"getting_started/configure_kdenlive/configuration_proxy_clips>` en :doc:`/"
"tips_and_tricks/tips_and_tricks/timeline_preview_rendering` in te schakelen "
"om vertraging bij voorbeeldweergave te verminderen."

#: ../../getting_started/installation.rst:78
msgid ""
"Video editing is in general relying heavily on CPU power. While Kdenlive has "
"render profiles with GPU support, timeline playback uses the CPU. Therefore, "
"the more powerful your CPU the better the playback performance of Kdenlive. "
"More and better GPU support is on the near-term roadmap."
msgstr ""
"Bewerken van video leunt in het algemeen zwaar op CPU power. Terwijl "
"Kdenlive profielen voor renderen heeft met ondersteuning van GPU, gebruikt "
"afspelen op de tijdlijn de CPU. Dus, hoe krachtiger uw CPU hoe beter de "
"presentatie van het afspelen van Kdenlive is. Meer en betere ondersteuning "
"van de GPU staat op nabije routekaart."

#: ../../getting_started/installation.rst:82
msgid "Kdenlive on Linux"
msgstr "Kdenlive op Linux"

#: ../../getting_started/installation.rst:84
msgid "Kdenlive can be installed on non-KDE Desktops without any issues."
msgstr ""
"Kdenlive kan zonder enig probleem geïnstalleerd worden op niet-KDE "
"bureaubladen."

#: ../../getting_started/installation.rst:86
msgid ""
"**Packages:** Minimum Ubuntu 22.04 for PPA. AppImage, Snap or Flatpak have "
"no such minimal requirements."
msgstr ""
"**Pakketten:** Minimaal Ubuntu 22.04 voor PPA. AppImage, Snap of Flatpak "
"hebben dit soort minimale vereisten niet."

#: ../../getting_started/installation.rst:91
msgid "Kdenlive on Windows"
msgstr "Kdenlive op Windows"

#: ../../getting_started/installation.rst:93
msgid ""
"Kdenlive runs only on 64bit version of Windows. Kdenlive runs on Windows 7 "
"and newer. We cannot guarantee that Kdenlive runs on server or embedded "
"Windows version."
msgstr ""
"Kdenlive werkt alleen op de 64bit versie van Windows. Kdenlive werkt op "
"Windows 7 en nieuwer. We kunnen niet beloven dat Kdenlive werkt op een "
"server of op de embedded Windows versie."

#: ../../getting_started/installation.rst:95
msgid "Kdenlive is available as an install and as a standalone version."
msgstr ""
"Kdenlive is beschikbaar in een versie om te installeren en als alleenstaand."

#: ../../getting_started/installation.rst:97
msgid ""
"Install version: Needs administrator rights and gets installed on your local "
"machine. It is also listed as a program."
msgstr ""
"Versie om te installeren: heeft rechten van systeembeheerder nodig en wordt "
"geïnstalleerd op uw lokale machine. Het wordt ook als een programma getoond."

#: ../../getting_started/installation.rst:99
msgid "It is available for all users on your computer."
msgstr "Het is beschikbaar voor alle gebruikers op uw computer."

#: ../../getting_started/installation.rst:101
msgid "The Kdenlive files are always located in the same folder."
msgstr "De bestanden van Kdenlive zitten altijd in dezelfde map."

#: ../../getting_started/installation.rst:103
msgid ""
"Standalone version: **Doesn't** need administrator rights and isn't "
"installed. It is **not** listed as a program. Is only accessible for the "
"user who has downloaded the file."
msgstr ""
"Alleenstaande versie: **heeft geen** admin-rechten nodig en is niet "
"geïnstalleerd. Het wordt **niet** in de lijst met programma's getoond. Het "
"alleen beschikbaar voor de gebruiker die het bestand heeft gedownload."

#: ../../getting_started/installation.rst:105
msgid "If you work with a normal user on your computer, you can use Kdenlive."
msgstr ""
"Als u als een normale gebruiker op uw computer werkt, dan kunt u Kdenlive "
"gebruiken."

#: ../../getting_started/installation.rst:107
msgid ""
"You can copy the Kdenlive folder on any external drive and run it on a "
"different computer without installing it. However, your personal settings "
"and downloads within Kdenlive are stored on the computer you work on."
msgstr ""
"U kunt de Kdenlive map naar een externe schijf kopiëren en het op een andere "
"computer laten werken zonder het te moeten installeren. Echter, uw "
"persoonlijke instellingen en in Kdenlive gedownloade bestanden worden "
"opgeslagen op de computer waarop u werkt."

#: ../../getting_started/installation.rst:111
msgid "Double click the downloaded file."
msgstr "Dubbelklik op het gedownloade bestand."

#: ../../getting_started/installation.rst:117
msgid "Kdenlive self-extracting archive"
msgstr "Kdenlive zichzelf uitpakkend archief"

#: ../../getting_started/installation.rst:120
msgid "Point to the folder you like to store the Kdenlive folder"
msgstr "Selecteer de map waarin u de Kdenlive map wilt opslaan."

#: ../../getting_started/installation.rst:126
msgid "Kdenlive bin folder"
msgstr "Kdenlive bin-map"

#: ../../getting_started/installation.rst:129
msgid ""
"To start Kdenlive navigate to the :file:`bin` folder and double-click :file:"
"`kdenlive.exe`. You can also create a shortcut to your Desktop for easy "
"access. Right-click on :file:`kdenlive.exe` and select :guilabel:`Send to ..."
"` and then :guilabel:`Desktop (create shortcut)`."
msgstr ""
"Om Kdenlive te starten navigeert u naar de :file:`bin-map` en dubbelklikt u  "
"op :file:`kdenlive.exe`. U kunt ook een sneltoets op uw Bureaublad aanmaken "
"voor gemakkelijker toegang. Klik rechts op :file:`kdenlive.exe`. en "
"selecteer :guilabel:`Verzenden naar ...` en daarna :guilabel:`Bureaublad "
"(sneltoets aanmaken)`."

#: ../../getting_started/installation.rst:135
msgid "Kdenlive in a Windows Domain"
msgstr "Kdenlive in een Windows Domein"

#: ../../getting_started/installation.rst:137
msgid ""
"If you want to use Kdenlive with domain users using Windows Active Directory "
"and/or Group Policies (GPOs), make sure all users have read/write access to "
"the following folders:"
msgstr ""
"Als u Kdenlive wilt gebruiken met domeingebruikers die Windows Active "
"Directory en/of Group Policies (GPO's) gebruiken, controleer dan of alle "
"gebruikers lees/schrijfrechten hebben in de volgende mappen:"

#: ../../getting_started/installation.rst:141
msgid "%AppData%\\\\kdenlive"
msgstr "%AppData%\\\\kdenlive"

#: ../../getting_started/installation.rst:143
msgid "%LocalAppData%\\\\kdenlive"
msgstr "%LocalAppData%\\\\kdenlive"

#: ../../getting_started/installation.rst:145
msgid "%LocalAppData%\\\\kdenliverc"
msgstr "%LocalAppData%\\\\kdenliverc"

#: ../../getting_started/installation.rst:147
msgid "%LocalAppData%\\\\kdenlive-layoutsrc"
msgstr "%LocalAppData%\\\\kdenlive-layoutsrc"

#: ../../getting_started/installation.rst:149
msgid "%LocalAppData%\\\\kxmlgui5\\\\kdenlive\\kdenliveui.rc"
msgstr "%LocalAppData%\\\\kxmlgui5\\\\kdenlive\\kdenliveui.rc"

#: ../../getting_started/installation.rst:151
msgid "%AppData%\\\\kdenlive\\\\.backup"
msgstr "%AppData%\\\\kdenlive\\\\.backup"

#: ../../getting_started/installation.rst:153
msgid "%LocalAppData%\\\\knewstuff3"
msgstr "%LocalAppData%\\\\knewstuff3"

#: ../../getting_started/installation.rst:155
msgid "Also make sure no GPO is blocking the access to these folders."
msgstr "Zorg ervoor dat geen GPO de toegang tot deze mappen blokkeert."

#: ../../getting_started/installation.rst:161
msgid "Kdenlive on macOS"
msgstr "Kdenlive op macOS"

#: ../../getting_started/installation.rst:163
msgid ""
"Kdenlive runs with Intel-based Macs on macOS 10.15 (Catalina)\\ [1]_ or "
"newer and on M1 (available on the |download_page|)."
msgstr ""
"Kdenlive werkt op Intel gebaseerde Mac's met macOS 10.15 (Catalina) \\ [1]_ "
"of nieuwer en op M1 (beschikbaar op de |download_page|)."

#: ../../getting_started/installation.rst:170
msgid "Installation Procedure"
msgstr "Installatie procedure"

#: ../../getting_started/installation.rst:176
msgid "MacOS download option"
msgstr "Downloadoptie MacOS"

#: ../../getting_started/installation.rst:178
msgid "Choose the option *Open with DiskImageMounter (Default)*."
msgstr "Selecteer de optie *Open with DiskImageMounter (Default)*."

#: ../../getting_started/installation.rst:184
msgid "MacOS DiskImageMounter"
msgstr "MacOS DiskImageMounter"

#: ../../getting_started/installation.rst:186
msgid ""
"When the dmg file is downloaded, the *DiskImageMounter* will open. Drag the "
"*Kdenlive* Logo into the *Applications* Folder."
msgstr ""
"als het dmg-bestand is gedownload, dan zal *DiskImageMounter* openen. Sleep "
"het *Kdenlive* Logo naar de map *Toepassingen*."

#: ../../getting_started/installation.rst:192
msgid "MacOS copy"
msgstr "MacOS kopie"

#: ../../getting_started/installation.rst:194
msgid "The files get copied."
msgstr "De bestanden worden gekopieerd."

#: ../../getting_started/installation.rst:200
msgid "MacOS check"
msgstr "MacOS controle"

#: ../../getting_started/installation.rst:202
msgid "MacOS will try to check the files for malware."
msgstr "MacOS zal proberen de bestanden te controleren op malware."

#: ../../getting_started/installation.rst:208
msgid "MacOS warning"
msgstr "MacOS waarschuwing"

#: ../../getting_started/installation.rst:210
msgid ""
"The message *“kdenlive\" cannot be opened, because Apple cannot search for "
"malware in it* will appear. Here you have to click :guilabel:`Show in "
"Finder`."
msgstr ""
"Het bericht *“kdenlive\" cannot be opened, because Apple cannot search for "
"malware in it* zal verschijnen. Hier moet u klikken op :guilabel:`Show in "
"Finder`."

#: ../../getting_started/installation.rst:216
msgid "MacOS right-click"
msgstr "MacOS klik rechts"

#: ../../getting_started/installation.rst:218
msgid ""
"The Finder opens. Now right click on *Kdenlive* and choose :guilabel:`Open`."
msgstr "De Finder opent. Klik nu op *Kdenlive* en selecteer :guilabel:`Open`."

#: ../../getting_started/installation.rst:224
msgid "MacOS open"
msgstr "MacOS openen"

#: ../../getting_started/installation.rst:226
msgid ""
"The message that Apple can't search for malware will appear again. Just "
"click on :guilabel:`Open` and Kdenlive will open up."
msgstr ""
"Het bericht dat Apple can't search for malware zal opnieuw verschijnen. Klik "
"gewoon op :guilabel:`Open` en Kdenlive zal openen."

#: ../../getting_started/installation.rst:232
msgid "Automatically Check for Updates"
msgstr "Automatische controle op Updates"

#: ../../getting_started/installation.rst:236
msgid ""
"Kdenlive automatically checks at startup for updates based on the app "
"version (no network connection needed)"
msgstr ""
"Kdenlive controleert automatisch bij opstarten op updates gebaseerd op de "
"programmaversie (geen netwerkverbinding nodig)"

#: ../../getting_started/installation.rst:242
msgid ""
"You can disable this function in :menuselection:`Menu --> Settings --> "
"Configure Kdenlive -->` :doc:`Environment</getting_started/"
"configure_kdenlive/configuration_environment>` by un-checking :guilabel:"
"`Check for updates`."
msgstr ""
"U kunt deze functie uitschakelen door in :menuselection:`Menu --> "
"Instellingen --> Kdenlive configureren` :doc:`Omgeving</getting_started/"
"configure_kdenlive/configuration_environment>` het :guilabel:`Op bijwerken "
"controleren` uit te schakelen."

#: ../../getting_started/installation.rst:248
msgid ""
"Due to QT6 compatibility the build system was switched to C++17 in January "
"2022 so minimum macOS requirement is macOS 10.15."
msgstr ""
"Vanwege compatibiliteit met Qt6 is het bouwsysteem omgeschakeld naar C++17 "
"in januari 2022 dus de minimale macOS versie is 10.15."

#, fuzzy
#~| msgid ""
#~| "Kdenlive runs with Intel based Macs on macOS 10.15 (Catalina)\\ [1]_ or "
#~| "newer and on M1 (available on the |download_page|)."
#~ msgid ""
#~ "Kdenlive runs with Intel-based Macs on macOS 10.15 (Catalina)\\ [1]_ or "
#~ "newer and on M1 (available on the |download_page|). Kdenlive runs with "
#~ "Intel-based Macs on macOS 10.15 (Catalina)\\ [1]_ or newer and on M1 "
#~ "(available on the |download_page|)."
#~ msgstr ""
#~ "Kdenlive draait op Intel gebaseerde Mac's onder macOS 10.15 (Catalina)\\ "
#~ "[1]_ of nieuwer en op M1 (beschikbaar op de |download_page|)."

#~ msgid ""
#~ "You can install Kdenlive in two different ways: Using an installer or as "
#~ "fully self-contained executable (Windows: standalone; Linux: appimage). "
#~ "On MacOS you can only use the installer version."
#~ msgstr ""
#~ "U kunt Kdenlive installeren op twee verschillende manieren: een "
#~ "installatieprogramma gebruiken of als volledig zelfvoorzienend "
#~ "uitvoerbaar programma (Windows: alleenstaand; Linux: appimage). Onder "
#~ "MacOS kunt u alleen de versie voor het installatieprogramma gebruiken."

#~ msgid "Configuration Information"
#~ msgstr "Configuratie-informatie"

#~ msgid ""
#~ "Kdenlive's application-wide persistent settings are stored in the "
#~ "following locations, depending on your platform."
#~ msgstr ""
#~ "De toepassingsbrede blijvende instellingen van Kdenlive worden opgeslagen "
#~ "in de volgende locaties, afhankelijk van uw platform."

#~ msgid "Linux"
#~ msgstr "Linux"

#~ msgid "Windows"
#~ msgstr "Vensters"

#~ msgid "Description"
#~ msgstr "Beschrijving"

#~ msgid ":file:`~/.config/kdenliverc`"
#~ msgstr ":file:`~/.config/kdenliverc`"

#~ msgid ":file:`%LocalAppData%\\\\kdenliverc`"
#~ msgstr ":file:`%LocalAppData%\\\\kdenliverc`"

#~ msgid ""
#~ "General settings of the application. Delete this and restart Kdenlive to "
#~ "reset the application to \"factory\" settings"
#~ msgstr ""
#~ "Algemene instellingen van de toepassing. Verwijder deze en herstart "
#~ "Kdenlive om de toepassing te restten naar \"factory\" instellingen"

#~ msgid ":file:`~/.config/kdenlive-appimagerc`"
#~ msgstr ":file:`~/.config/kdenlive-appimagerc`"

#~ msgid ""
#~ "Linux AppImage only: contains the general settings of the application"
#~ msgstr ""
#~ "Alleen Linux AppImage: bevat de algemene instellingen van de toepassing"

#~ msgid ":file:`~/.config/session/kdenlive_*`"
#~ msgstr ":file:`~/.config/session/kdenlive_*`"

#~ msgid "temporary session info"
#~ msgstr "tijdelijke sessie-informatie"

#~ msgid ":file:`~/.cache/kdenlive`"
#~ msgstr ":file:`~/.cache/kdenlive`"

#~ msgid ":file:`%LocalAppData%\\\\kdenlive`"
#~ msgstr ":file:`%LocalAppData%\\\\kdenlive`"

#~ msgid ""
#~ "cache location storing audio and video thumbnails, and proxy clips, user "
#~ "defined titles, LUTS, lumas, shortcuts"
#~ msgstr ""
#~ "cache-locatie voor opslaan van audio- en video-miniaturen en proxy-clips, "
#~ "gebruikergedefinieerde titels, LUTS, luma's, sneltoetsen"

#~ msgid ":file:`~/.local/share/kdenlive`"
#~ msgstr ":file:`~/.local/share/kdenlive`"

#~ msgid ":file:`%AppData%\\\\kdenlive`"
#~ msgstr ":file:`%AppData%\\\\kdenlive`"

#~ msgid ""
#~ "contains downloaded: effects, export, library, opencv models, profiles, "
#~ "speech models, and titles"
#~ msgstr ""
#~ "bevat gedownloade:, effecten, geëxporteerd, bibliotheek, opencv-modellen, "
#~ "profielen, spraakmodellen en titels"

#~ msgid ":file:`~/.local/share/kdenlive/lumas`"
#~ msgstr ":file:`~/.local/share/kdenlive/lumas`"

#~ msgid ":file:`%LocalAppData%\\\\kdenlive\\\\lumas`"
#~ msgstr ":file:`%LocalAppData%\\\\kdenlive\\\\lumas`"

#~ msgid ""
#~ "lumas folder contains the files used for :doc:`Wipe </compositing/"
#~ "transitions/wipe>`"
#~ msgstr ""
#~ "lumas-map bevat de bestanden gebruikt voor :doc:`Wipe </compositing/"
#~ "transitions/wipe>`"

#~ msgid ":file:`~/.local/share/kdenlive/.backup`"
#~ msgstr ":file:`~/.local/share/kdenlive/.backup`"

#~ msgid ":file:`%AppData%\\\\kdenlive\\\\.backup`"
#~ msgstr ":file:`%AppData%\\\\kdenlive\\\\.backup`"

#~ msgid "Auto Save Recovery files"
#~ msgstr "Automatisch opgeslagen herstelbestanden"

#~ msgid ":file:`~/.config/kdenlive-layoutsrc`"
#~ msgstr ":file:`~/.config/kdenlive-layoutsrc`"

#~ msgid ":file:`%LocalAppData%\\\\kdenlive-layoutsrc`"
#~ msgstr ":file:`%LocalAppData%\\\\kdenlive-layoutsrc`"

#~ msgid "contains the layout settings"
#~ msgstr "bevat de instellingen voor indeling"

#~ msgid ":file:`~/.local/share/kxmlgui5/kdenlive/kdenliveui.rc`"
#~ msgstr ":file:`~/.local/share/kxmlgui5/kdenlive/kdenliveui.rc`"

#~ msgid ":file:`%LocalAppData%\\\\kxmlgui5\\kdenlive\\\\kdenliveui.rc`"
#~ msgstr ":file:`%LocalAppData%\\\\kxmlgui5\\kdenlive\\\\kdenliveui.rc`"

#~ msgid "contains UI configuration, if your UI is broken, delete this file"
#~ msgstr ""
#~ "bevat UI-configuratie, als uw UI niet goed werkt, verwijder dit bestand"

#~ msgid ":file:`~/.local/share/knewstuff3`"
#~ msgstr ":file:`~/.local/share/knewstuff3`"

#~ msgid ":file:`%LocalAppData%\\\\knewstuff3`"
#~ msgstr ":file:`%LocalAppData%\\\\knewstuff3`"

#~ msgid "contains LUT definition"
#~ msgstr "bevat LUT-definitie"

#~ msgid ":file:`~/.local/share/kdenlive/speechmodels`"
#~ msgstr ":file:`~/.local/share/kdenlive/speechmodels`"

#~ msgid ":file:`%AppData%\\\\kdenlive\\\\speechmodels`"
#~ msgstr ":file:`%AppData%\\\\kdenlive\\\\speechmodels`"

#~ msgid "contains the VOSK models downloaded"
#~ msgstr "bevat de gedownloade VOSK modellen"

#~ msgid ":file:`~/.local/share/kdenlive/opencvmodels`"
#~ msgstr ":file:`~/.local/share/kdenlive/opencvmodels`"

#~ msgid ":file:`%AppData%\\\\kdenlive\\\\opencvmodels`"
#~ msgstr ":file:`%AppData%\\\\kdenlive\\\\opencvmodels`"

#~ msgid "contains the OpenCV models downloaded"
#~ msgstr "bevat de gedownloade OpenCV modellen"

#~ msgid ":file:`~/.local/share/kdenlive/venv`"
#~ msgstr ":file:`~/.local/share/kdenlive/venv`"

#~ msgid ":file:`%LocalAppData%\\\\kdenlive\\\\venv`"
#~ msgstr ":file:`%LocalAppData%\\\\kdenlive\\\\venv`"

#~ msgid "contains the Python virtual environment (venv)"
#~ msgstr "bevat de Python virtuele omgeving (venv)"

#~ msgid ":file:`$HOME/.cache/hugginface`"
#~ msgstr ":file:`$HOME/.cache/hugginface`"

#~ msgid ":file:`C:\\\\Users\\\\<username>\\\\.cache\\\\huggingface`"
#~ msgstr ":file:`C:\\\\Users\\\\<username>\\\\.cache\\\\huggingface`"

#~ msgid "contains the SeamlessM4T models for Whisper"
#~ msgstr "bevat de SeamlessM4T-modellen voor Whisper"

#~ msgid ""
#~ "To reach the above folders: :kbd:`Windows+R` then copy above path into "
#~ "the window."
#~ msgstr ""
#~ "Om de bovenstaande mappen te bereiken: :kbd:`Windows+R` kopieer daarna "
#~ "het bovenstaande pad in het venster."

#~ msgid "**Notes**"
#~ msgstr "**Notities**"

#~ msgid "macOS"
#~ msgstr "macOS"

#~ msgid "**Windows**"
#~ msgstr "**Windows**"

#~ msgid ""
#~ "To start Kdenlive navigate to the `bin folder` and double click Kdenlive"
#~ msgstr ""
#~ "Om Kdenlive te starten ga u naar de `map bin` en dubbelklikt u op Kdenlive"

#~ msgid "Non-KDE Desktops"
#~ msgstr "Niet-KDE bureaubladen"

#~ msgid "Contents"
#~ msgstr "Inhoud"

#~ msgid ""
#~ "**Kdenlive** and **MLT** can compile and run under macOS. There is no "
#~ "stable macOS version yet, but you can help to have one soon by testing "
#~ "our daily macOS version (available on the `download <https://kdenlive.org/"
#~ "download/>`_ page). If you have experience with building software for "
#~ "macOS, please help us!"
#~ msgstr ""
#~ "**Kdenlive** en **MLT** kunnen gecompileerd worden en draaien onder "
#~ "macOS. Er is nog geen stabiele macOS versie, maar u kunt helpen er "
#~ "spoedig een te hebben door onze dagelijkse macOS versie te testen "
#~ "(beschikbaar op de pagina `download <https://kdenlive.org/download/>`_). "
#~ "Als u ervaring hebt met bouwen van software voor macOS, help ons dan!"

#~ msgid ":file:`%LOCALAPPDATA%\\kdenlive\\cache`"
#~ msgstr ":file:`%LOCALAPPDATA%\\kdenlive\\cache`"

#~ msgid ":file:`~/.local/share/kdenlive/HD`"
#~ msgstr ":file:`~/.local/share/kdenlive/HD`"

#~ msgid ":file:`%PROGRAMFILES%\\kdenlive\\bin\\data\\kdenlive\\lumas`"
#~ msgstr ":file:`%PROGRAMFILES%\\kdenlive\\bin\\data\\kdenlive\\lumas`"
